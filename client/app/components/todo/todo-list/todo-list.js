import angular from 'angular';
import uiRouter from 'angular-ui-router';
import todoListComponent from './todo-list.component';

let todoListModule = angular.module('todoList', [
    uiRouter
])

.component('todoList', todoListComponent)
.name;
export default todoListModule;

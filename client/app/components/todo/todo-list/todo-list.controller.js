class TodoListController {

  constructor() {
    this.name = 'todoList';
  }
  removeItem($index) {
      this.onRemoveTodo({
        $event: {
          index: $index
        }
      });
  }
}

export default TodoListController;

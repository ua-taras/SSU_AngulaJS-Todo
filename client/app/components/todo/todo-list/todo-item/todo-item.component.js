import template from './todo-item.html';
import controller from './todo-item.controller';

let todoItemComponent = {
  bindings: {
    todo: '<',
    onRemoveItem: '&'
  },
  template,
  controller
};

export default todoItemComponent;

class TodoService {
    constructor($q) {
        this.$q = $q;
    }
      getTodos() {
    let todos = [
      { text: 'learn angular', done: true },
      { text: 'build an angular app', done: false },
      { text: 'modify', done: true },
      { text: 'test', done: false },
      { text: 'close', done: false }
    ];
    return this.$q.when(todos);
  }
}

TodoService.$inject = ['$q'];

export default TodoService;

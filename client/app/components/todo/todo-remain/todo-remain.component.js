import template from './todo-remain.html';
import controller from './todo-remain.controller';

let todoRemainComponent = {
  bindings: {
    remaining: '<'
  },
  template,
  controller
};

export default todoRemainComponent;

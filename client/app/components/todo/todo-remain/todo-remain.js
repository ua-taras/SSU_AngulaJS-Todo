import angular from 'angular';
import todoRemainComponent from './todo-remain.component';

let todoRemainModule = angular.module('todoRemain', [])

.component('todoRemain', todoRemainComponent)
.name;
export default todoRemainModule;

import template from './todo-archive.html';
import controller from './todo-archive.controller';

let todoArchiveComponent = {
  bindings: {
    onAddToArchive: '&'
  },
  template,
  controller
};

export default todoArchiveComponent;

class TodoController {
    constructor(TodoService) {
        'ngInject';
        this.todoService = TodoService;
    }
    
    $onInit() {
        this.newTodo = {
            text: '',
            done: false
        };

        this.filter = {
            text: ''
        };

        this.todos = [];
        this.todoService.getTodos()
            .then(response => this.todos = response);
    }

    addTodo({ todo }) {
        if (!todo) return;
        this.todos.unshift(todo);
        this.newTodo = {
            text: '',
            done: false
        };
    }

    removeTodo({ index }) {
        this.todos.splice(index, 1);
    }

    remaining() {
        return this.todos.filter(item => item.done === false).length;
    }

    addToArchive() {
        this.todos = this.todos.filter(item => !item.done);
    }

}

export default TodoController;

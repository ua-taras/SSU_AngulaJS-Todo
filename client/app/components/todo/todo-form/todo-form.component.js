import template from './todo-form.html';
import controller from './todo-form.controller';

let todoFormComponent = {
  bindings: {
    todo: '<',
	  onAddTodo: '&'
  },
  template,
  controller
};

export default todoFormComponent;

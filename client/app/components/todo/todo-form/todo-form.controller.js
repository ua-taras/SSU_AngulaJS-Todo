class TodoFormController {
    $onChanges(changes) {
      if (changes.todo) {
        this.todo = Object.assign({}, this.todo);
      }
    }
    onSubmit() {
      if (!this.todo.text) return;
      this.onAddTodo({
        $event: {
          todo: this.todo
        }
    });
	}
}

export default TodoFormController;

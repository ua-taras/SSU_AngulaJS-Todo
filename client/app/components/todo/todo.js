import angular from 'angular';
import TodoService from './todo.service';
import uiRouter from 'angular-ui-router';
import todoComponent from './todo.component';

let todoModule = angular.module('todo', [
    uiRouter
])
    .config(($stateProvider) => {
        "ngInject";

        $stateProvider
            .state('todo', {
                url: '/',
                component: 'todo'
            });
    })
    .component('todo', todoComponent)
    .service('TodoService', TodoService)
    .name;

export default todoModule;

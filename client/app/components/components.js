import angular from 'angular';
import Home from './home/home';
import About from './about/about';
import Todo from './todo/todo';
import TodoList from './todo/todo-list/todo-list';
import todoItem from './todo/todo-list/todo-item/todo-item';
import todoForm from './todo/todo-form/todo-form';
import todoArchive from './todo/todo-archive/todo-archive';
import todoRemain from './todo/todo-remain/todo-remain';

let componentModule = angular.module('app.components', [
  Home,
  About,
  Todo,
  TodoList,
  todoItem,
  todoForm,
  todoArchive,
  todoRemain
])

.name;

export default componentModule;
